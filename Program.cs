﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReaderPreferences
{
    class Program
    {
        
        static void Main(string[] args)
        {

            var conn = connect();

            try
            {
                //key book; value set of readers
                var readersOfBooks = new Dictionary<int, HashSet<int>>();

                var cmdHasRead = new NpgsqlCommand("SELECT bookid, readerID FROM hasRead", conn);
                var readerHasRead = cmdHasRead.ExecuteReader();

                while (readerHasRead.Read())
                {
                    int bookID = readerHasRead.GetInt32(0);
                    if (!(readersOfBooks.ContainsKey(bookID)))
                    {
                        readersOfBooks.Add(bookID, new HashSet<int>());

                    }
                    readersOfBooks[bookID].Add(readerHasRead.GetInt32(1));

                }
                readerHasRead.Close();

                // key genre; value set of readers
                var readersOfGenres = new Dictionary<int, HashSet<int>>();

                var cmdBook = new NpgsqlCommand("SELECT bookid, genreid FROM book", conn);
                var readerBook = cmdBook.ExecuteReader();

                while (readerBook.Read())
                {
                    int genreID = readerBook.GetInt32(1);
                    if (!(readersOfGenres.ContainsKey(genreID)))
                    {
                        readersOfGenres.Add(genreID, new HashSet<int>());
                    }
                    foreach (int bookid in readersOfBooks.Keys)
                    {
                        if (bookid == readerBook.GetInt32(0))
                        {
                            readersOfGenres[genreID].UnionWith(readersOfBooks[bookid]);
                        }
                    }
                }
                readerBook.Close();

                //key reader value age
                var readerAge = new Dictionary<int, int>();

                var cmdReader = new NpgsqlCommand("SELECT readerid, age FROM reader", conn);
                var readerReader = cmdReader.ExecuteReader();

                while (readerReader.Read())
                {
                    readerAge.Add(readerReader.GetInt32(0), readerReader.GetInt32(1));
                }


                //calculate averages
                var averageAgePerGenre = new Dictionary<int, float>();
                foreach (int genre in readersOfGenres.Keys)
                {
                    int sum = 0;
                    foreach (int reader in readersOfGenres[genre])
                    {
                        sum += readerAge[reader];
                    }
                    averageAgePerGenre.Add(genre, sum * 1f / readersOfGenres[genre].Count);
                }
                readerReader.Close();

                var cmdGenre = new NpgsqlCommand("SELECT genreid, name FROM genre", conn);
                var readerGenre = cmdGenre.ExecuteReader();

                Console.WriteLine("Average age of reader per genre:");

                while (readerGenre.Read())
                {
                    int genreID = readerGenre.GetInt32(0);
                    if (averageAgePerGenre.ContainsKey(genreID))
                    {
                        Console.WriteLine(readerGenre.GetValue(1) + " -- " + averageAgePerGenre[genreID]);
                    }
                    else
                    {
                        Console.WriteLine(readerGenre.GetValue(1) + " -- " + 0);
                    }
                }
                readerGenre.Close();

                conn.Close();
            } catch (Exception ex)
            {
                Console.WriteLine("Unexpected error: " + ex.Message);
            }

            Console.ReadKey();

        }

        private static NpgsqlConnection connect()
        {
            String connString = "Host=localhost;Port=5432;Username=postgres;Password=100;Database=ReadingPreferences";
            NpgsqlConnection conn = null;
            try
            {
                conn = new NpgsqlConnection(connString);
                conn.Open();
            } catch (Exception ex)
            {
                Console.WriteLine("Unexpected error: " + ex.Message);
                Console.ReadKey();
                Environment.Exit(-1);
            }
            return conn;
        }
    }
}
